package com.vkuzub.rotatescreen;

import android.app.Activity;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.View;
import android.widget.*;

import java.util.*;

/**
 * Created by Vyacheslav on 10.09.2014.
 */
public class MainActivity extends Activity implements View.OnClickListener {

    private Button btnSet, btnSettings;
    private Spinner spnrOrientations;

    private int position = 0; // позиция в spinner

    private String[] orientations;
    private boolean isOptionsShowed;
    private Fragment optionsFragment;

    private static Map<String, Integer> orientationsMap = null;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);

        orientations = getResources().getStringArray(R.array.orientations);
        optionsFragment = new OptionsFragment();

        initMap();
        initWidgets();

        readOptions();

    }

    private void initMap() {
        orientationsMap = new HashMap<String, Integer>();
        for (int i = 0; i < orientations.length; i++) {
            orientationsMap.put(orientations[i], i);
        }
    }

    private void initWidgets() {
        btnSet = (Button) findViewById(R.id.btnSet);
        btnSet.setOnClickListener(this);

        btnSettings = (Button) findViewById(R.id.btnSettings);
        btnSettings.setOnClickListener(this);


    }

    private void initSpinner(String[] modeTitles) {
        ArrayAdapter<String> spnrAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, modeTitles);
        spnrOrientations = (Spinner) findViewById(R.id.spnrOrientations);
        spnrOrientations.setAdapter(spnrAdapter);
        spnrOrientations.setSelection(0);
        spnrOrientations.setOnItemSelectedListener(MySpinnerItemClickListener);
    }

    //посмотреть статус
    private boolean isOrientationLocked() {
        int value = 1;
        try {
            value = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.ACCELEROMETER_ROTATION);
        } catch (Settings.SettingNotFoundException e) {
            value = 1;
        }
        return value == 0;
    }

    //Заблокировать поворот акселерометра
    private void setOrientationLock(boolean lock) {
        android.provider.Settings.System.putInt(getContentResolver(),
                Settings.System.ACCELEROMETER_ROTATION,
                lock ? 0 : 1);
    }

    //Повернуть экран вручную
    private void setUserOrientation(int orientation) {
        setOrientationLock(true); // блокировка системного поворота
        android.provider.Settings.System.putInt(getContentResolver(),
                Settings.System.USER_ROTATION, orientation);
    }

    //закрыть список после нажатия ок
    private void showSettings() {
        getFragmentManager().beginTransaction().add(R.id.flSettings, optionsFragment, "options").commit();
        isOptionsShowed = true;
        spnrOrientations.setEnabled(false);
        btnSet.setEnabled(false);
    }

    private void closeSettings() {
        getFragmentManager().beginTransaction().remove(optionsFragment).commit();
        isOptionsShowed = false;
        spnrOrientations.setEnabled(true);
        btnSet.setEnabled(true);
        readOptions();
    }

    private void readOptions() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        ArrayList<String> modesList = new ArrayList<String>(sp.getStringSet("listModes", new HashSet<String>(Arrays.asList(orientations))));
        Collections.sort(modesList);


        if (modesList.size() == 0) {
            Toast.makeText(getApplicationContext(), "Modes can't be less than 1", Toast.LENGTH_SHORT).show();
            initSpinner(orientations);
            //default preference values
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            preferences.edit().clear().commit();
            return;
        }

        //default values
        if (modesList.size() == 4) {
            initSpinner(orientations);
            return;
        }

        String[] dynamicOrientations = new String[modesList.size()];
        for (int i = 0; i < modesList.size(); i++) {
            dynamicOrientations[i] = orientations[Integer.parseInt(modesList.get(i))];
        }

        initSpinner(dynamicOrientations);
    }


    private final AdapterView.OnItemSelectedListener MySpinnerItemClickListener = new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            String pos = (String) adapterView.getItemAtPosition(i);
            position = orientationsMap.get(pos);
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSet:
                setUserOrientation(position);
                break;
            case R.id.btnSettings:
                if (!isOptionsShowed) {
                    showSettings();
                } else {
                    closeSettings();
                }
                break;
        }
    }
}