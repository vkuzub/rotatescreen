# RotateScreen

Приложение поворачивает экран Android-девайса вручную. 
Требуется Android 3.0 (api11) и выше.

![Screen1](https://bytebucket.org/vkuzub/rotatescreen/raw/65f4fbe310a392876c2a42d68eea443792ed6d37/screenshots/screenshot1.png?token=4967683cc9c1778a2597be82262843f4484af15d)

![Screen2](https://bytebucket.org/vkuzub/rotatescreen/raw/65f4fbe310a392876c2a42d68eea443792ed6d37/screenshots/screenshot2.png?token=85c0eff4351fbeec4cc94e302085e5505b1d4846)